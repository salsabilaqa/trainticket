<?php

namespace App\Models;

use CodeIgniter\Model;

class GuideModel extends Model
{
    protected $table      = 'jadwal_tiket';
    public function cariTiket($asal, $tujuan, $tgl_berangkat)
    {
        return
            $this->db->table('jadwal_tiket')
            ->join('stasiun AS Asal', 'jadwal_tiket.asal=Asal.id_stasiun')
            ->join('stasiun AS Tujuan', 'jadwal_tiket.tujuan=Tujuan.id_stasiun')
            ->where('asal', $asal)
            ->where('tujuan', $tujuan)
            ->like('tgl_berangkat', $tgl_berangkat)
            ->get()->getResultArray();
    }
}
