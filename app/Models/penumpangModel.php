<?php

namespace App\Models;

use CodeIgniter\Model;

class PenumpangModel extends Model
{
    protected $table      = 'penumpang';
    protected $primaryKey = 'id_penumpang';
    protected $allowedFields = [
        'nama_penumpang', 'no_id', 'kode', 'no_kursi'
    ];

    public function getPenumpangReservasi()
    {
        return
            $this->db->table('penumpang')
            ->join('reservasi', 'penumpang.kode=reservasi.id_reservasi')
            ->get()->getResultArray();
    }
    public function getPenumpangDetail($id_penumpang)
    {
        return
            $this->db->table('penumpang')
            ->join('reservasi', 'penumpang.kode=reservasi.id_reservasi')
            ->where("jadwal_tiket.id_ka='" . $id_penumpang . "'")
            ->get()->getResultArray();
    }
    public function data_penumpang()
    {
        return $this->db->table($this->table)->join('reservasi', 'penumpang.kode=reservasi.id_reservasi')->get()->getResultArray();
    }

    public function update_data($data, $id_penumpang)
    {
        $query = $this->db->table('penumpang')->update($data, array('id_penumpang' => $id_penumpang));
        return $query;
    }
    public function delete_data($id_penumpang)
    {
        $query = $this->db->table($this->table)->delete(array('id_penumpang' => $id_penumpang));
        return $query;
    }
} //end class