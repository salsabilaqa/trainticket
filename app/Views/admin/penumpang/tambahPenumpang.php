<?php $this->extend('template'); ?>
<?php $this->section('isi'); ?>
<div class="container">
    <div class="card mt-3">
        <div class="card-header">
            <b><?= $title ?></b>
        </div>
        <div class="card-body">
            <form action="<?php echo base_url('penumpang/simpan') ?>" method="post">
                <div class="form-group row">
                    <input type="hidden" name="id_penumpang">
                    <label for="nama" class="col-sm-3 label">Kode Booking</label>
                    <div class="col-sm-4">
                        <input type="text" name="kode" class="form-control" value="TK">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 label">Nama Penumpang</label>
                    <div class="col-sm-4">
                        <input type="text" name="nama_penumpang" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 label">No Identitas</label>
                    <div class="col-sm-4">
                        <input type="text" name="no_id" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 label">No Kursi</label>
                    <div class="col-sm-4">
                        <input type="text" name="no_kursi" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-4">
                        <input type="submit" value="submit" class="btn btn-info">
                    </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->endSection(); ?>