<?php $this->extend('template'); ?>
<?php $this->section('isi'); ?>
<div class="container">
    <div class="card mt-3">
        <div class="card-header">
            <b><?= $title ?></b>
        </div>
        <div class="card-body">
            <form action="<?php echo base_url('penumpang/update') ?>" method="post">
                <div class="form-group row">
                    <input type="hidden" name="id_penumpang" value="<?= $penumpang['id_penumpang']; ?>">
                    <label for="nama" class="col-sm-2 label">Kode Booking</label>
                    <div class="col-sm-4">
                        <?php
                        foreach ($reservasi as $r) :
                            $data = [
                                'class' => 'form-control',
                                'value' => '<?= $penumpang["kode"]; ?>'
                            ];
                            echo form_input('kode', $r["kode"], $data);
                        endforeach;
                        ?>
                        <!-- <input type="text" name="kode" class="form-control" value="<?= $penumpang['kode']; ?>"> -->
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 label">Nama Penumpang</label>
                    <div class="col-sm-4">
                        <input type="text" name="nama_pemesan" class="form-control" value="<?= $penumpang['nama_pemesan']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 label">No Identitas</label>
                    <div class="col-sm-4">
                        <input type="text" name="no_identitas" class="form-control" value="<?= $penumpang['no_identitas']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 label">No Kursi</label>
                    <div class="col-sm-4">
                        <input type="text" name="no_kursi" class="form-control" value="<?= $penumpang['no_kursi']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-4">
                        <input type="submit" value="submit" class="btn btn-info">
                    </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->endSection(); ?>