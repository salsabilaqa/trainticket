<?php $this->extend('template'); ?>
<?php $this->section('isi'); ?>
<div class="container">
    <div class="card mt-3">
        <div class="card-header">
            <b><?= $title ?></b>
        </div>
        <div class="card-body">
            <a href="<?= site_url('penumpang/tambah'); ?>" class="btn btn-info"><i class="fas fa-plus-circle"></i> Tambah </a>
            <br><br>
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Kode Booking</th>
                    <th>Nama Penumpang</th>
                    <th>No Identitas</th>
                    <th>No Kursi</th>
                    <th><i class="fas fa-cogs"></i></th>
                </tr>
                <?php
                $no = 1;
                foreach ($penumpang as $key) : ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $key['kode']; ?></td>
                        <td><?php echo $key['nama_penumpang']; ?></td>
                        <td><?php echo $key['no_id']; ?></td>
                        <td><?php echo $key['no_kursi']; ?></td>
                        <td>
                            <a href="/penumpang/edit/<?php echo $key['id_penumpang']; ?>" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                            <a href="/penumpang/delete/<?php echo $key['id_penumpang']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin akan menghapus data ini ?')"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<?php $this->endSection(); ?>