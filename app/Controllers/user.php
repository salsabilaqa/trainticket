<?php

namespace App\Controllers;

use App\Models\JadwalModel;
use App\Models\StasiunModel;
use App\Models\GuideModel;

class user extends BaseController
{
    protected $jadwalModel;
    protected $stasiunModel;
    protected $guideModel;
    public function __construct()
    {
        $this->jadwalModel = new JadwalModel();
        $this->stasiunModel = new StasiunModel();
    }
    public function pesan()
    {
        return view('user/pesan.php');
    }
    public function cari()
    {
        // $asal = $this->input->post('asal', true);
        // $tujuan = $this->input->post('tujuan', true);
        // $tgl_berangkat = $this->input->post('tgl_berangkat', true);
        // $stasiun = $this->stasiunModel->getStasiun();
        // $jadwal = $this->jadwalModel->cariTiket($asal, $tujuan, $tgl_berangkat);
        // $data = [
        //     'title' => 'Data Jadwal',
        //     'jadwal' => $jadwal,
        //     'asal' => $stasiun,
        //     'tujuan' => $stasiun
        // ];
        echo view('user/cari.php');
    }
    public function table()
    {
        return view('user/table.php');
    }
    public function data()
    {
        return view('user/data.php');
    }
    public function ringkasan()
    {
        return view('user/ringkasan.php');
    }
    public function pembayaran()
    {
        return view('user/pembayaran.php');
    }
    public function pembayaran1()
    {
        return view('user/pembayaran1.php');
    }
}
