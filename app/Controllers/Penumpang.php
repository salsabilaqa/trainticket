<?php

namespace App\Controllers;

use App\Models\PenumpangModel;
use App\Models\ReservasiModel;

class Penumpang extends BaseController
{
    protected $jadwalModel;
    protected $stasiunModel;
    public function __construct()
    {
        $this->penumpangModel = new PenumpangModel();
        $this->reservasiModel = new ReservasiModel();
    }

    public function index()
    {
        $penumpang = $this->penumpangModel->data_penumpang();
        $data = [
            'title' => 'Data Penumpang',
            'penumpang' => $penumpang
        ];
        echo view('admin/penumpang/index', $data);
    }

    public function detail($id_penumpang)
    {
        // $stasiun = $this->stasiunModel->getStasiun();
        $penumpang = $this->penumpangModel->getPenumpangDetail($id_penumpang);
        $data['penumpang'] = $penumpang[0];
        // $data = [
        //     'jadwal' => $jadwal,
        //     'asal' => $stasiun, 'tujuan' => $stasiun
        // ];
        echo view('admin/penumpang/detailPenumpang', $data);
    }

    public function tambah()
    {
        $reservasi = $this->reservasiModel->getReservasi();
        $data = [
            'title' => 'Tambah Data Penumpang',
            'kode' => $reservasi
        ];
        echo view('admin/penumpang/tambahPenumpang', $data);
    }
    public function simpan()
    {
        $this->penumpangModel->save([
            'nama_penumpang'           => $this->request->getVar('nama_penumpang'),
            'no_id'         => $this->request->getVar('no_id'),
            'kode'  => $this->request->getVar('kode'),
            'no_kursi'     => $this->request->getVar('no_kursi')
        ]);
        echo '<script>
            alert("Data berhasil ditambahkan");
            window.location="' . base_url('Penumpang') . '"
        </script>';
    }

    public function edit($id_penumpang)
    {
        $reservasi = $this->reservasiModel->getReservasi();
        $penumpang = $this->penumpangModel->data_penumpang($id_penumpang)[0];
        $data = [
            'title'    => 'Ubah Data Jadwal Tiket',
            'jadwal' => $penumpang,
            'kode' => $reservasi
        ];
        echo view('admin/penumpang/editPenumpang', $data);
    }
    public function update()
    {
        $id_penumpang = $this->request->getVar('id_penumpang');
        $data = [
            'nama_penumpang'           => $this->request->getVar('nama_penumpang'),
            'no_id'         => $this->request->getVar('no_id'),
            'kode'  => $this->request->getVar('kode'),
            'no_kursi'     => $this->request->getVar('no_kursi')
        ];

        $this->penumpangModel->update_data($data, $id_penumpang);
        echo '<script>
            alert("Data berhasil diubah");
            window.location="' . base_url('Penumpang') . '"
        </script>';
    }

    public function delete($id_penumpang)
    {
        $this->penumpangModel->delete_data($id_penumpang);
        echo '<script>
            alert("Data berhasil dihapus");
            window.location="' . base_url('Penumpang') . '"
        </script>';
    }
}
