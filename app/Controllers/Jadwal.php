<?php

namespace App\Controllers;

use App\Models\JadwalModel;
use App\Models\StasiunModel;

class Jadwal extends BaseController
{
    protected $jadwalModel;
    protected $stasiunModel;
    public function __construct()
    {
        $this->jadwalModel = new JadwalModel();
        $this->stasiunModel = new StasiunModel();
    }

    public function index()
    {
        $jadwal = $this->jadwalModel->getJdwlStasiun();
        $data = [
            'title' => 'Data Jadwal',
            'jadwal' => $jadwal
        ];
        echo view('admin/jadwal/index2', $data);
    }

    public function detail($id_ka)
    {
        // $stasiun = $this->stasiunModel->getStasiun();
        $jadwal = $this->jadwalModel->getJdwlDetail($id_ka);
        $data['jadwal'] = $jadwal[0];
        // $data = [
        //     'jadwal' => $jadwal,
        //     'asal' => $stasiun, 'tujuan' => $stasiun
        // ];
        echo view('admin/jadwal/detailJadwal', $data);
    }

    public function tambah()
    {
        $stasiun = $this->stasiunModel->getStasiun();
        $data = [
            'title' => 'Tambah Data Jadwal Tiket',
            'asal' => $stasiun, 'tujuan' => $stasiun
        ];
        echo view('admin/jadwal/tambahJadwal2', $data);
    }
    public function simpan()
    {
        $this->jadwalModel->save([
            'nama_ka'        => $this->request->getVar('nama_ka'),
            'asal'           => $this->request->getVar('asal'),
            'tujuan'         => $this->request->getVar('tujuan'),
            'tgl_berangkat'  => $this->request->getVar('tgl_berangkat'),
            'tgl_sampai'     => $this->request->getVar('tgl_sampai'),
            'jam_berangkat'  => $this->request->getVar('jam_berangkat'),
            'jam_tiba'       => $this->request->getVar('jam_tiba'),
            'kelas'          => $this->request->getVar('kelas'),
            'harga'          => $this->request->getVar('harga'),
            'stok'           => $this->request->getVar('stok')
        ]);
        echo '<script>
            alert("Data berhasil ditambahkan");
            window.location="' . base_url('Jadwal') . '"
        </script>';
    }

    public function edit($id_ka)
    {
        $stasiun = $this->stasiunModel->getStasiun();
        $jadwal = $this->jadwalModel->data_jadwal($id_ka)[0];
        $data = [
            'title'    => 'Ubah Data Jadwal Tiket',
            'jadwal' => $jadwal,
            'asal' => $stasiun, 'tujuan' => $stasiun
        ];
        echo view('admin/jadwal/editJadwal2', $data);
    }
    public function update()
    {
        $id_ka = $this->request->getVar('id_ka');
        $data = [
            'nama_ka'        => $this->request->getVar('nama_ka'),
            'asal'           => $this->request->getVar('asal'),
            'tujuan'         => $this->request->getVar('tujuan'),
            'tgl_berangkat'  => $this->request->getVar('tgl_berangkat'),
            'tgl_sampai'     => $this->request->getVar('tgl_sampai'),
            'jam_berangkat'  => $this->request->getVar('jam_berangkat'),
            'jam_tiba'       => $this->request->getVar('jam_tiba'),
            'kelas'          => $this->request->getVar('kelas'),
            'harga'          => $this->request->getVar('harga'),
            'stok'           => $this->request->getVar('stok')
        ];

        $this->jadwalModel->update_data($data, $id_ka);
        echo '<script>
            alert("Data berhasil diubah");
            window.location="' . base_url('Jadwal') . '"
        </script>';
    }

    public function delete($id_ka)
    {
        $this->jadwalModel->delete_data($id_ka);
        echo '<script>
            alert("Data berhasil dihapus");
            window.location="' . base_url('Jadwal') . '"
        </script>';
    }
}
